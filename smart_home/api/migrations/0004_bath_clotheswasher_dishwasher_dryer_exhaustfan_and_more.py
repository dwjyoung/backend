# Generated by Django 4.0.4 on 2022-04-20 08:24

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_remove_room_thermostat_id_remove_sensor_room_id_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bath',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('type', models.TextField(default='', max_length=50)),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='ClothesWasher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='DishWasher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Dryer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='ExhaustFan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='HotWaterHeater',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='LightBulb',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Microwave',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Oven',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Refrigerator',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Stove',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='TV',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=50, unique=True)),
                ('open_close', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)])),
                ('type', models.TextField(default='', max_length=50)),
                ('cost', models.FloatField()),
                ('usage', models.FloatField()),
            ],
        ),
        migrations.DeleteModel(
            name='Sensor',
        ),
        migrations.AddField(
            model_name='acunit',
            name='cost',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='acunit',
            name='open_close',
            field=models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(1)]),
        ),
        migrations.AddField(
            model_name='acunit',
            name='usage',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
