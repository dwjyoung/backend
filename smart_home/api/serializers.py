from rest_framework.serializers import ModelSerializer
from .models import ACunit, Thermostat, Room, LightBulb, ExhaustFan, Refrigerator, Microwave, HotWaterHeater, Stove, Oven, TV, Dryer, Bath, DishWasher, ClothesWasher


class ACunitSerializer(ModelSerializer):
    class Meta:
        model = ACunit
        fields = '__all__'


class ThermostatSerializer(ModelSerializer):
    class Meta:
        model = Thermostat
        fields = '__all__'


class RoomSerializer(ModelSerializer):
    class Meta:
        model = Room
        fields = '__all__'

class LightBulbSerializer(ModelSerializer):
    class Meta:
        model = LightBulb
        fields = '__all__'


class ExhaustFanSerializer(ModelSerializer):
    class Meta:
        model = ExhaustFan
        fields = '__all__'

class RefrigeratorSerializer(ModelSerializer):
    class Meta:
        model = Refrigerator
        fields = '__all__'


class MicrowaveSerializer(ModelSerializer):
    class Meta:
        model = Microwave
        fields = '__all__'


class HotWaterHeaterSerializer(ModelSerializer):
    class Meta:
        model = HotWaterHeater
        fields = '__all__'


class StoveSerializer(ModelSerializer):
    class Meta:
        model = Stove
        fields = '__all__'


class OvenSerializer(ModelSerializer):
    class Meta:
        model = Oven
        fields = '__all__'

class TVSerializer(ModelSerializer):
    class Meta:
        model = TV
        fields = '__all__'


class DryerSerializer(ModelSerializer):
    class Meta:
        model = Dryer
        fields = '__all__'


class BathSerializer(ModelSerializer):
    class Meta:
        model = Bath
        fields = '__all__'


class DishWasherSerializer(ModelSerializer):
    class Meta:
        model = DishWasher
        fields = '__all__'


class ClothesWasherSerializer(ModelSerializer):
    class Meta:
        model = ClothesWasher
        fields = '__all__'
