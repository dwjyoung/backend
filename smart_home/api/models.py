from django.db import models
from django.core.validators import MaxValueValidator

# Create your models here.
class Thermostat(models.Model):
    # ac_unit_id = models.ForeignKey(ACunit,related_name= '+', on_delete=models.CASCADE)
    name = models.TextField(max_length=50, unique=True)

    def __str__(self):
        return self.ac_unit_id[0:20]

class Room(models.Model):
    name = models.TextField(max_length=50, unique=True)
    # thermostat_id = models.ForeignKey(Thermostat,on_delete=models.CASCADE, default="", unique=False)

    def __str__(self):
        return self.name[0:20]
    
#Electric Sensors

class LightBulb(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20]

class ExhaustFan(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20]

class ACunit(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20]

class Refrigerator(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()
    
    def __str__(self):
        return self.name[0:20] 

class Microwave(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()   
    
    def __str__(self):
        return self.name[0:20]

class HotWaterHeater(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20]

class Stove(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20]

class Oven(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 

class TV(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    type = models.TextField(max_length=50, default="", unique=False)
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 

class Dryer(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 


#Water and Power Sensors
class Bath(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    type = models.TextField(max_length=50, default="", unique=False)
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 

class DishWasher(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 

class ClothesWasher(models.Model):
    name = models.TextField(max_length=50, unique=True)
    open_close = models.IntegerField(default=int(1), unique=False, validators=[MaxValueValidator(1)])
    cost = models.FloatField()
    usage = models.FloatField()

    def __str__(self):
        return self.name[0:20] 

