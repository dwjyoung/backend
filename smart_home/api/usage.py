from django.db import models

def elec_cost(kw: float):
    return 0.12 * kw

def water_cost(gal: float):
    return 0.0034 * gal

class ElectricAppliance(models.Model):
    def _get_cost(self,kw: float):
        self.cost = elec_cost(kw)

class WaterAppliance(models.Model):
    def _get_cost(self,gal: float):
        self.cost = water_cost(gal)

class Microwave(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 1.1

        if active:
            if weekend:
                self.usage = .5 * kw_hr
            else:
                self.usage = (20 / 60) * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class LightBulb(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 0.06

        if active:
            if weekend:
                self.usage = 15.5 * kw_hr
            else:
                self.usage = 5 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class ExhaustFan(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 0.03

        if active:
            if weekend:
                self.usage = 3 * kw_hr
            else:
                self.usage = 1 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class ACunit(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 3.5

        if active:
            if weekend:
                self.usage = 5 * kw_hr
            else:
                self.usage = 3 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class Refrigerator(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 0.150

        if active:
            if weekend:
                self.usage = 8 * kw_hr
            else:
                self.usage = 4 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class HotWaterHeater(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 4.5

        if active:
            if weekend:
                self.usage = 8 * kw_hr
            else:
                self.usage = 5.2 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class Stove(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 3.5

        if active:
            if weekend:
                self.usage = .5 * kw_hr
            else:
                self.usage = .25 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class Oven(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 4.0

        if active:
            if weekend:
                self.usage = 1 * kw_hr
            else:
                self.usage = 0.75 * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class TV(ElectricAppliance):
    def __init__(self, type, weekend: bool = False, active: bool = False): 
        
        if type == 'living-room':
            kw_hr = 0.636

            if active:
                if weekend:
                    self.usage = 4 * kw_hr
                else:
                    self.usage = 8 * kw_hr
            else:
                self.usage = 0
        else:
            kw_hr = 0.1
            if active:
                if weekend:
                    self.usage = 4 * kw_hr
                else:
                    self.usage = 8 * kw_hr
            else:
                self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class Dryer(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 3

        if active:
            if weekend:
                self.usage = .5 * kw_hr
            else:
                self.usage = (20 / 60) * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class DishWasher(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = 1.8

        if active:
            if weekend:
                self.usage = .5 * kw_hr
            else:
                self.usage = (20 / 60) * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class ClothesWasher(ElectricAppliance):
    def __init__(self, weekend: bool = False, active: bool = False): 
        kw_hr = .5

        if active:
            if weekend:
                self.usage = .5 * kw_hr
            else:
                self.usage = (20 / 60) * kw_hr

        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class ClothesWasher(WaterAppliance):
    def __init__(self, active: bool = False):
        if active:
            self.usage = 11.4
        else:
            self.usage = 0

    def get_cost(self):
        return self._get_cost(self.usage)

class DishWasher(WaterAppliance):
    def __init__(self, active: bool = False):
        if active:
            self.usage = 3.4
        else:
            self.usage = 0
    
    def get_cost(self):
        return self._get_cost(self.usage)

class Bath(WaterAppliance):
    def __init__(self, weekend: bool = False, active: bool = False):
        if active:
            if weekend:
                self.usage = 165
            else:
                self.usage = 110
        else:
            self.usage = 0
    
    def get_cost(self):
        return self._get_cost(self.usage)