from django.contrib import admin

# Register your models here.

from .models import ACunit
from .models import Thermostat
from .models import Room
from .models import LightBulb
from .models import ExhaustFan
from .models import Refrigerator
from .models import Microwave
from .models import HotWaterHeater
from .models import Stove
from .models import Oven
from .models import TV
from .models import Dryer
from .models import Bath
from .models import DishWasher
from .models import ClothesWasher



admin.site.register(ACunit)
admin.site.register(Thermostat)
admin.site.register(Room)
admin.site.register(LightBulb)
admin.site.register(ExhaustFan)
admin.site.register(Refrigerator)
admin.site.register(Microwave)
admin.site.register(HotWaterHeater)
admin.site.register(Stove)
admin.site.register(Oven)
admin.site.register(TV)
admin.site.register(Dryer)
admin.site.register(Bath)
admin.site.register(DishWasher)
admin.site.register(ClothesWasher)
