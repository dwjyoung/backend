from django.urls import path
from . import views

urlpatterns = [
    path('', views.getRoutes, name="routes"),

    path('thermostats', views.getThermostats, name="Thermostats"),
    path('thermostats/<str:pk>', views.getThermostat, name="Thermostat"),

    path('rooms', views.getRooms, name="Rooms"),
    path('rooms/<str:pk>', views.getRoom, name="Room"),

    path('ac-units', views.getAcUnits, name="AC Units"),
    path('ac-units/<str:pk>/update', views.getAcUnit, name="Update AC Unint"),
    path('ac-units/<str:pk>', views.updateAcUnit, name="AC Unit"),

    path('light-bulb', views.getLightBulbs, name="Light Bulbs"),
    path('light-bulb/<str:pk>/update', views.getLightBulb, name="Update Light Bulb"),
    path('light-bulb/<str:pk>', views.updateLightBulb, name="Light Bulb"),

    path('exhaust-fan', views.getExhaustFans, name="Exhaust Fans"),
    path('exhaust-fan/<str:pk>/update', views.getExhaustFan, name="Update Exhaust Fan"),
    path('exhaust-fan/<str:pk>', views.updateExhaustFan, name="Exhaust Fan"),

    path('microwave', views.getMicrowaves, name="Microwaves"),
    path('microwave/<str:pk>/update', views.getMicrowave, name="Update Microwave"),
    path('microwave/<str:pk>', views.updateMicrowave, name="Microwave"),

    path('hot-water-heater', views.getHotWaterHeaters, name="Hot Water Heaters"),
    path('hot-water-heater/<str:pk>/update', views.getHotWaterHeater, name="Update Hot Water Heater"),
    path('hot-water-heater/<str:pk>', views.updateHotWaterHeater, name="Hot Water Heater"),

    path('stove', views.getStoves, name="Stoves"),
    path('stove/<str:pk>/update', views.getStove, name="Update Stove"),
    path('stove/<str:pk>', views.updateStove, name="Stove"),

    path('oven', views.getOvens, name="Ovens"),
    path('oven/<str:pk>/update', views.getOven, name="Update Oven"),
    path('oven/<str:pk>', views.updateOven, name="Oven"),

    path('tv', views.getTVs, name="Bed Room TVs"),
    path('tv/<str:pk>/update', views.getTV, name="Update Bed Room TV"),
    path('tv/<str:pk>', views.updateTV, name="Bed Room TV"),

    path('dryer', views.getDryers, name="Dryers"),
    path('dryer/<str:pk>/update', views.getDryer, name="Update Dryer"),
    path('dryer/<str:pk>', views.updateDryer, name="Dryer"),

    path('bath', views.getBaths, name="Bath"),
    path('bath/<str:pk>/update', views.getBath, name="Update Bath"),
    path('bath/<str:pk>', views.updateBath, name="Bath"),

    path('dish-washer', views.getDishWashers, name="Dish Washers"),
    path('dish-washer/<str:pk>/update', views.getDishWasher, name="Update Dish Washer"),
    path('dish-washer/<str:pk>', views.updateDishWasher, name="Dish Washer"),

    path('clothes-washer', views.getClothesWashers, name="Clothes Washers"),
    path('clothes-washer/<str:pk>/update', views.getClothesWasher, name="Update Clothes Washer"),
    path('clothes-washer/<str:pk>', views.updateClothesWasher, name="Clothes Washer"), 
]